namespace API.Entities
{
    public class RealmUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
    }
}